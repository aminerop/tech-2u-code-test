## Setup and installation

-  `git cone https://bitbucket.org/aminerop/tech-2u-code-test`
- `cd tech-2u-code-test`
- `yarn install`
if on macOS machine willing to run iOS version, run-first `yarn run pod-install`, as for android the auto-linking takes care of that step

for running ios :
- `yarn run ios:dev`

for running android : 
- `yarn run android:dev`

as you noticed the commands are suffixed with the environment variable, I did the setup for adding other environments (staging, production...).
for that I used `react-native-config`. also, I used that to store API URL and sentry DSN.

## Features
- I used sentry for tracking and monitoring bugs cause this is crucial for any app to keep the app stable for most users
- for managing the state I used react-query since its convenient for remote state and offers many UX and performance wins out of the box
- persisting user's state so we serve cache first then fetch the response from API, all of that thanks to react-query as well
- inform the user in case of no internet connection by showing a sticky alert at the top
- the data we present to the users are shown are human-readable, instead of relying on labels to describe what the data is about we present data in a meaningful way
- a skeleton when we are in loading state because spinners are lame, also I handled the case where we fail at fetching data
- users can click on phone/email/website/address and get redirected to the appropriate app (i.e: the mail app will be opened if a user clicked on email )
- a search feature cause it is always handy
- and last not least a beautiful UI with a little spark of animations

## Code
 I tried to follow a clean project structure where the file belongs where it should be, it's easier that way to navigate and browse the code for refactoring later, and separate concerns as much as I can.
 normally I do rely on a library that helps for building a design system so I can build my own UI components on top of that, so here I used just the normal stylesheet cause I knew I won't need that many components, but not gonna lie working with stylesheet is straight to the point and faster but it does not scale unfortunately.
 and of course, I used typescript to keep my sanity lol.
 
## Deemo Tiiiiime

Home Screen            |  Details Screen
:-------------------------:|:-------------------------:
![Home Screen](https://user-images.githubusercontent.com/33012060/148662233-811bdc8d-904d-47c8-961d-a256f0b061ae.png)  |  ![Details Screen](https://user-images.githubusercontent.com/33012060/148662220-fe46d66e-ba74-4009-bb36-d7b016b01812.png)

