import React, {useEffect, useRef} from "react";
import {StatusBar} from "react-native";
import {SafeAreaProvider} from "react-native-safe-area-context";
import {NavigationContainer} from "@react-navigation/native";
import {QueryClient, QueryClientProvider} from "react-query";
import AsyncStorage from "@react-native-async-storage/async-storage";
import {persistQueryClient} from "react-query/persistQueryClient-experimental";
import {createAsyncStoragePersistor} from "react-query/createAsyncStoragePersistor-experimental";
import * as Sentry from "@sentry/react-native";
import {addEventListener} from "@react-native-community/netinfo";
import DropdownAlert from "react-native-dropdownalert";
import Routes from "routes";

const queryClient = new QueryClient({
    defaultOptions: {
        queries: {
            cacheTime: 1000 * 60 * 60 * 24, // 24 hours
        },
    },
});

const asyncStoragePersistor = createAsyncStoragePersistor({
    storage: AsyncStorage,
});

persistQueryClient({
    queryClient,
    persistor: asyncStoragePersistor,
});

const App = () => {
    const dropDownAlertRef = useRef<DropdownAlert>();

    useEffect(() => {
        const subscriber = addEventListener(async (netInfo) => {
            if (!netInfo.isInternetReachable) {
                dropDownAlertRef.current?.alertWithType(
                    "error",
                    "No Internet",
                    "Please check your internet connection",
                    {},
                    Number.MAX_VALUE,
                );
            }
        });
        return subscriber;
    }, []);

    return (
        <NavigationContainer>
            <QueryClientProvider client={queryClient}>
                <StatusBar barStyle="light-content" animated />
                <SafeAreaProvider>
                    <Routes />
                    <DropdownAlert
                        ref={(ref: DropdownAlert) =>
                            (dropDownAlertRef.current = ref)
                        }
                    />
                </SafeAreaProvider>
            </QueryClientProvider>
        </NavigationContainer>
    );
};

export default Sentry.wrap(App);
