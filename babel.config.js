module.exports = {
    presets: ["module:metro-react-native-babel-preset"],
    plugins: [
        [
            "module-resolver",
            {
                extensions: [
                    ".js",
                    ".jsx",
                    ".ts",
                    ".tsx",
                    ".android.js",
                    ".android.tsx",
                    ".ios.js",
                    ".ios.tsx",
                ],
                alias: {
                    routes: "./routes",
                    screens: "./screens",
                    core: "./core",
                    components: "./components",
                },
            },
        ],
        "react-native-reanimated/plugin",
    ],
};
