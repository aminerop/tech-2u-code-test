import React, {useCallback, useMemo, useState} from "react";
import {
    View,
    FlatList,
    StyleSheet,
    ListRenderItemInfo,
    TextInput,
    SafeAreaView,
    ScrollView,
} from "react-native";
import MaterialCommunityIcons from "react-native-vector-icons/MaterialCommunityIcons";
import {useGetUsers} from "core/api";
import {User} from "core/types";
import Row from "components/Row";
import Card from "components/Card";
import Error from "components/Error";

const Home = () => {
    const [searchQuery, setSearchQuery] = useState("");
    const {data, status, refetch} = useGetUsers();
    const keyExtractor = (item: any) => item.id;

    const renderItem = ({item}: ListRenderItemInfo<User>) => {
        return <Row user={item} />;
    };

    const users = useMemo(() => {
        const filtredItems = data?.filter((item) => {
            return (
                item.name.toLowerCase().indexOf(searchQuery) !== -1 ||
                item.email.toLowerCase().indexOf(searchQuery) !== -1
            );
        });

        return searchQuery === "" ? data : filtredItems;
    }, [searchQuery, data]);

    const ErrorCallback = useCallback(() => {
        return (
            <Error
                title="Oh no!"
                subTitle="We couldnt find any friends for you. but hey you can try again"
                icon="alert-circle-outline"
                retry={refetch}
            />
        );
        // eslint-disable-next-line react-hooks/exhaustive-deps
    }, []);

    const renderBody = useCallback(() => {
        if (status === "loading") {
            return (
                <ScrollView style={styles.list}>
                    {[1, 2, 3, 4, 5].map((item) => (
                        <Row key={item} />
                    ))}
                </ScrollView>
            );
        } else if (status === "error") {
            return (
                <Error
                    title="Oops!"
                    subTitle="We cant seem to find what you are looking for."
                    icon="cloud-off-outline"
                    retry={refetch}
                />
            );
        } else {
            return (
                <FlatList
                    data={users}
                    keyExtractor={keyExtractor}
                    renderItem={renderItem}
                    contentContainerStyle={styles.list}
                    keyboardDismissMode="on-drag"
                    keyboardShouldPersistTaps="never"
                    ListEmptyComponent={ErrorCallback}
                />
            );
        }
        // eslint-disable-next-line react-hooks/exhaustive-deps
    }, [users, status]);

    return (
        <SafeAreaView style={styles.container}>
            <View style={styles.background}>
                <View style={styles.header} />
                <View style={styles.body}>
                    <Card activeOpacity={1} style={styles.info}>
                        <TextInput
                            placeholder={"Search"}
                            autoCapitalize="none"
                            onChangeText={setSearchQuery}
                            value={searchQuery}
                            style={styles.search}
                            placeholderTextColor="rgba(3, 3, 3, 0.3)"
                        />
                        <MaterialCommunityIcons
                            name="magnify"
                            size={22}
                            color="rgba(3, 3, 3, 0.6)"
                        />
                    </Card>
                    {renderBody()}
                </View>
            </View>
        </SafeAreaView>
    );
};

const styles = StyleSheet.create({
    container: {
        flex: 1,
        backgroundColor: "#5F56D7",
    },
    background: {
        flex: 1,
        backgroundColor: "white",
    },
    header: {
        height: "10%",
        width: "100%",
        backgroundColor: "#5F56D7",
        borderBottomEndRadius: 8,
        borderBottomStartRadius: 8,
    },
    search: {
        fontSize: 18,
        paddingLeft: 5,
        marginRight: 5,
        flex: 1,
    },
    body: {
        position: "absolute",
        top: "5%",
        right: 0,
        left: 0,
        bottom: 0,
    },
    list: {
        marginTop: 8,
        flexGrow: 1,
    },
    info: {
        borderRadius: 8,
        flexDirection: "row",
        marginBottom: 16,
        alignItems: "center",
    },
});

export default Home;
