import * as React from "react";
import {
    View,
    Text,
    StyleSheet,
    ScrollView,
    Linking,
    TouchableOpacity,
} from "react-native";
import {useRoute} from "@react-navigation/native";
import FastImage from "react-native-fast-image";
import {SafeAreaView} from "react-native-safe-area-context";
import {openComposer} from "react-native-email-link";
import call from "react-native-phone-call";
import Card from "components/Card";
import {useGetuserDetail} from "core/api";
import {AppRoute} from "routes/types";
import Info from "components/Info";
import {reportException} from "core/sentry";
import {Severity} from "@sentry/react-native";
import Skeleton from "components/Skeleton";

const Details = () => {
    const {params} = useRoute<AppRoute>();
    const {data, status} = useGetuserDetail(params?.id);

    const {id, name, email, company, username, address} = data || {};
    const website = data && "https://" + data.website;
    const phone = data?.phone.split(" ")[0];
    const isError = status === "error";

    const sendMail = () => {
        openComposer({to: email}).catch((error) =>
            reportException(error, Severity.Warning),
        );
    };

    const makeCall = () => {
        phone &&
            call({
                number: phone,
                prompt: true,
            }).catch((error) => reportException(error, Severity.Warning));
    };

    const openWebsite = async () => {
        const supported = website && (await Linking.canOpenURL(website));
        if (supported) {
            Linking.openURL(website).catch((error) =>
                reportException(error, Severity.Warning),
            );
        }
    };

    const openGoogleMaps = () => {
        address &&
            Linking.openURL(
                `https://www.google.com/maps/@${address.geo.lng},${address.geo.lat}`,
            ).catch((error) => reportException(error, Severity.Warning));
    };

    return (
        <SafeAreaView style={styles.container}>
            <View style={styles.header} />
            <View style={styles.footer} />
            <View style={styles.body}>
                <Card activeOpacity={1} style={styles.info}>
                    <Skeleton show={!id} style={styles.avatar}>
                        <FastImage
                            style={styles.avatar}
                            source={{
                                uri: id
                                    ? `https://randomuser.me/api/portraits/${
                                          id % 2 === 0 ? "men" : "women"
                                      }/${id}.jpg`
                                    : "",
                            }}
                        />
                    </Skeleton>
                    <View style={styles.description}>
                        <Skeleton
                            height={15}
                            width={"60%"}
                            show={!name && !username}>
                            <Text style={styles.name}>{name}</Text>
                            <Text style={styles.username}>
                                (a.k.a {username})
                            </Text>
                        </Skeleton>
                    </View>
                    <Skeleton height={15} width={"40%"} show={!company?.name}>
                        <Text style={styles.company}>@{company?.name}</Text>
                    </Skeleton>
                </Card>
                <ScrollView bounces={false} contentContainerStyle={{flex: 1}}>
                    <Info onPress={sendMail} name="email" value={email} />
                    <Info onPress={makeCall} name="phone" value={phone} />
                    <Info onPress={openWebsite} name="link" value={website} />
                    <View style={styles.location}>
                        <View style={{opacity: 0.7}}>
                            <Text style={styles.label}>Visit us</Text>
                            <Skeleton
                                height={10}
                                width={"100%"}
                                show={!address}>
                                <Text style={styles.address}>
                                    {address?.street} - {address?.suite}
                                </Text>
                            </Skeleton>
                            <Skeleton
                                height={10}
                                width={"100%"}
                                show={!address?.city}>
                                <Text style={styles.address}>
                                    {address?.city}
                                </Text>
                            </Skeleton>
                            <Skeleton
                                height={10}
                                width={"100%"}
                                show={!address?.zipcode}>
                                <Text style={styles.address}>
                                    {address?.zipcode}
                                </Text>
                            </Skeleton>
                        </View>
                        <Skeleton
                            show={!address}
                            style={[styles.map, {height: "80%", flex: 0.5}]}>
                            <TouchableOpacity
                                activeOpacity={0.8}
                                style={{flex: 0.7}}
                                onPress={openGoogleMaps}>
                                <FastImage
                                    style={styles.map}
                                    source={{
                                        uri: "https://maps.googleapis.com/maps/api/staticmap?center=51.477222,0&zoom=14&size=400x400&key=AIzaSyA3kg7YWugGl1lTXmAmaBGPNhDW9pEh5bo&signature=ciftxSv4681tGSAnes7ktLrVI3g=",
                                    }}
                                />
                            </TouchableOpacity>
                        </Skeleton>
                    </View>
                </ScrollView>
            </View>
        </SafeAreaView>
    );
};

const styles = StyleSheet.create({
    container: {
        flex: 1,
        alignItems: "center",
        backgroundColor: "#5F56D7",
    },
    header: {
        height: "20%",
        width: "100%",
        backgroundColor: "#5F56D7",
    },
    footer: {
        height: "80%",
        width: "100%",
        backgroundColor: "white",
        borderTopEndRadius: 24,
        borderTopStartRadius: 24,
    },
    body: {
        position: "absolute",
        top: "15%",
        right: 0,
        left: 0,
        bottom: 0,
    },
    info: {
        alignItems: "center",
        marginBottom: 32,
    },
    avatar: {
        width: 80,
        height: 80,
        borderRadius: 40,
        marginTop: "-20%",
    },
    description: {
        flexDirection: "row",
        alignItems: "center",
        marginTop: 8,
        justifyContent: "center",
        flexWrap: "wrap",
    },
    name: {
        fontSize: 21,
        fontWeight: "bold",
        marginRight: 4,
    },
    company: {
        marginTop: 3,
        fontSize: 16,
        opacity: 0.7,
    },
    username: {
        fontSize: 16,
        fontWeight: "bold",
        color: "#5F56D7",
        opacity: 0.9,
    },
    label: {
        marginTop: 8,
        marginBottom: 30,
        fontSize: 22,
    },
    address: {
        marginBottom: 2,
        fontSize: 16,
    },
    location: {
        flex: 1,
        flexDirection: "row",
        justifyContent: "space-evenly",
    },
    map: {
        flex: 0.8,
        borderRadius: 12,
        marginVertical: 22,
    },
});

export default Details;
