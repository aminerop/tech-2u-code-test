import {RouteProp} from "@react-navigation/native";
import {StackNavigationProp} from "@react-navigation/stack";

export type AppNavigtorParamList = {
    Home: undefined;
    Details: {id: number};
};

export type AppNavigation = StackNavigationProp<
    AppNavigtorParamList,
    keyof AppNavigtorParamList
>;

export type AppRoute = RouteProp<
    AppNavigtorParamList,
    keyof AppNavigtorParamList
>;
