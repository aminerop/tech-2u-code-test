import * as React from "react";
import {createStackNavigator} from "@react-navigation/stack";
import {AppNavigtorParamList} from "routes/types";
import Home from "screens/home";
import Details from "screens/details";

const Stack = createStackNavigator<AppNavigtorParamList>();
const Routes = () => {
    return (
        <Stack.Navigator
            screenOptions={{
                headerTransparent: true,
                headerBackTitleVisible: false,
                headerTintColor: "#fff",
                title: "",
            }}>
            <Stack.Screen name="Home" component={Home} />
            <Stack.Screen name="Details" component={Details} />
        </Stack.Navigator>
    );
};

export default Routes;
