import env from "react-native-config";

const config = {
    isProd: process.env.NODE_ENV === "production",
    env: process.env.NODE_ENV,
    sentry: env.SENTRY_DSN,
    apiUrl: env.BASE_API_URL,
};

export default config;
