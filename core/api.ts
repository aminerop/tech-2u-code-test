import {useEffect} from "react";
import {useQuery} from "react-query";
import {Severity} from "@sentry/react-native";
import {User} from "core/types";
import config from "core/config";
import {reportException} from "core/sentry";

enum APIRoutes {
    users = "users",
}

async function get(path: string) {
    const response = await fetch(`${config.apiUrl}${path}`);
    if (response.status >= 200 && response.status < 400) {
        return response.json();
    } else {
        return Promise.reject(new Error(response.statusText));
    }
}

const useGetUsers = () => {
    const {data, status, error, refetch} = useQuery<User[], Error>(
        APIRoutes.users,
        () => get(APIRoutes.users),
        {notifyOnChangeProps: "tracked"},
    );

    useEffect(() => {
        if (error) {
            reportException(error, Severity.Error);
        }
    }, [error]);

    return {data, status, error, refetch};
};

const useGetuserDetail = (id?: number) => {
    const {data, status, error, refetch} = useQuery<User, Error>(
        [APIRoutes.users, id],
        () => get(APIRoutes.users + "/" + id),
        {notifyOnChangeProps: "tracked", enabled: !!id},
    );

    useEffect(() => {
        if (error) {
            reportException(error, Severity.Error);
        }
    }, [error]);

    return {data, status, error, refetch};
};

export {useGetUsers, useGetuserDetail};
