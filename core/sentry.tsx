import React, {useContext, useEffect} from "react";
import {
    setJSExceptionHandler,
    setNativeExceptionHandler,
} from "react-native-exception-handler";
import config from "core/config";
import {captureException, init, Severity} from "@sentry/react-native";

init({
    enableAutoSessionTracking: true,
    dsn: config.sentry,
    environment: config.env,
});

function reportException(err: Error, logger: Severity) {
    if (!config.isProd) {
        console.log(err);
        return;
    }
    if (err && logger) {
        try {
            return captureException(err, {level: logger});
        } catch (error) {}
    }
}

const nativeErrorHandler = (error: Error) => {
    reportException(error, Severity.Critical);
};

const jsErrorHandler = (error: Error, isFatal: boolean) => {
    if (!config.isProd) {
        // react-native-exception-handler redirects console.error to call this, and React calls
        // console.error without an exception when prop type validation fails, so this ends up
        // being called with no arguments when the error handler is enabled in dev mode.
        console.log(error);
        return;
    }
    reportException(error, isFatal ? Severity.Fatal : Severity.Error);
};

setJSExceptionHandler(jsErrorHandler, true);
setNativeExceptionHandler((_exceptionString) => {
    // This is your custom global error handler
    // You do stuff like hit google analytics to track crashes.
    // or hit a custom api to inform the dev team.
    // NOTE: alert or showing any UI change via JS
    // WILL NOT WORK in case of NATIVE ERRORS.
    nativeErrorHandler(new Error(_exceptionString));
});

export {reportException};
