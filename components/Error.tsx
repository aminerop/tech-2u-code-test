import React from "react";
import {View, StyleSheet, Text, TouchableOpacity} from "react-native";
import MaterialCommunityIcons from "react-native-vector-icons/MaterialCommunityIcons";
import Animated, {
    LightSpeedInLeft,
    Layout,
    LightSpeedOutRight,
} from "react-native-reanimated";

interface ErrorProps {
    retry: () => void;
    title: string;
    subTitle: string;
    icon: string;
}

const Error = ({title, subTitle, icon, retry}: ErrorProps) => {
    return (
        <Animated.View
            entering={LightSpeedInLeft}
            layout={Layout.springify()}
            exiting={LightSpeedOutRight}
            style={styles.container}>
            <MaterialCommunityIcons
                name={icon}
                size={80}
                color="rgba(3, 3, 3, 0.6)"
            />
            <Text style={styles.title}>{title}</Text>
            <Text style={styles.subTitle}>{subTitle}</Text>
            <TouchableOpacity
                onPress={() => retry()}
                activeOpacity={0.8}
                style={styles.tryAgainBtn}>
                <Text style={styles.ctaText}>Try again</Text>
            </TouchableOpacity>
        </Animated.View>
    );
};

export default Error;

const styles = StyleSheet.create({
    container: {
        flex: 1,
        justifyContent: "center",
        alignItems: "center",
        marginHorizontal: 30,
    },
    title: {
        fontSize: 28,
        fontWeight: "bold",
        marginBottom: 6,
    },
    subTitle: {
        fontSize: 18,
        fontWeight: "500",
        textAlign: "center",
        marginBottom: 20,
    },
    tryAgainBtn: {
        width: "100%",
        padding: 12,
        borderRadius: 999,
        backgroundColor: "#5F56D7",
    },
    ctaText: {
        fontSize: 20,
        fontWeight: "500",
        textAlign: "center",
        color: "white",
    },
});
