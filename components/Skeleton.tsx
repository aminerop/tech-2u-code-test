import React, {useEffect} from "react";
import {ViewStyle, ColorValue, StyleProp} from "react-native";
import Animated, {
    Easing,
    Extrapolate,
    interpolate,
    useAnimatedStyle,
    useSharedValue,
    withRepeat,
    withTiming,
} from "react-native-reanimated";

export interface SkeletonProps {
    animate?: boolean;
    width?: string | number;
    height?: string | number;
    rounded?: number;
    color?: ColorValue;
    opacity?: number;
    spaceX?: number;
    spaceY?: number;
    style?: StyleProp<ViewStyle>;
    show?: boolean;
}

const Skeleton = ({
    width = 20,
    height = 5,
    rounded = 10,
    color = "rgb(210, 210, 210)",
    opacity = 1,
    spaceX = 4,
    spaceY = 4,
    show,
    children,
    style,
}: React.PropsWithChildren<SkeletonProps>) => {
    const animation = useSharedValue(0);

    useEffect(() => {
        animation.value = withRepeat(
            withTiming(1, {
                duration: 1000,
                easing: Easing.bezier(0.4, 0, 0.6, 1),
            }),
            show ? -1 : 0,
            false,
        );
        // eslint-disable-next-line react-hooks/exhaustive-deps
    }, [show]);

    const animatedStyles = useAnimatedStyle(() => {
        const opc = interpolate(
            animation.value,
            [0, 1],
            [0.6, 0.4],
            Extrapolate.CLAMP,
        );
        return {
            opacity: opc,
        };
    });

    return show ? (
        <Animated.View
            style={[
                {
                    width,
                    height,
                    borderRadius: rounded,
                    backgroundColor: color,
                    opacity,
                    marginHorizontal: spaceX,
                    marginVertical: spaceY,
                },
                style,
                animatedStyles,
            ]}
        />
    ) : (
        <>{children}</>
    );
};

export default Skeleton;
