import * as React from "react";
import {
    Text,
    View,
    StyleSheet,
    GestureResponderEvent,
    TouchableOpacity,
} from "react-native";
import MaterialCommunityIcons from "react-native-vector-icons/MaterialCommunityIcons";
import Clipboard from "@react-native-clipboard/clipboard";
import Card from "components/Card";
import Skeleton from "components/Skeleton";

interface InfoProps {
    value?: string;
    name: string;
    onPress: () => void;
}

const Info = ({name, value, onPress}: InfoProps) => {
    const copyToClipboard = (e: GestureResponderEvent) => {
        value && Clipboard.setString(value);
        e.stopPropagation();
    };

    return (
        <Card onPress={() => value && onPress()} style={styles.container}>
            <View style={{flexDirection: "row"}}>
                <Skeleton show={!value} height={12} width={"80%"}>
                    <MaterialCommunityIcons
                        name={name}
                        size={22}
                        color="#5F56D7"
                    />
                    <Text style={styles.text}>{value}</Text>
                </Skeleton>
            </View>
            <TouchableOpacity onPress={copyToClipboard}>
                <MaterialCommunityIcons
                    name="content-copy"
                    size={22}
                    color="rgba(0, 0, 0, 0.7)"
                />
            </TouchableOpacity>
        </Card>
    );
};

const styles = StyleSheet.create({
    text: {
        fontSize: 18,
        fontWeight: "500",
        marginLeft: 8,
    },
    container: {
        flexDirection: "row",
        justifyContent: "space-between",
        marginBottom: 12,
    },
});

export default Info;
