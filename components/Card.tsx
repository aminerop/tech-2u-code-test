import React from "react";
import {
    ViewStyle,
    StyleSheet,
    TouchableOpacity,
    View,
    TouchableOpacityProps,
} from "react-native";

const Card = ({
    style,
    onPress,
    children,
    activeOpacity = 0.8,
}: React.PropsWithChildren<TouchableOpacityProps>) => {
    return (
        <TouchableOpacity activeOpacity={activeOpacity} onPress={onPress}>
            <View style={[styles.container, style]}>{children}</View>
        </TouchableOpacity>
    );
};

export default Card;

const styles = StyleSheet.create({
    container: {
        marginHorizontal: 22,
        padding: 16,
        backgroundColor: "#fff",
        borderRadius: 16,
        shadowColor: "#000",
        shadowOffset: {
            width: 0,
            height: 5,
        },
        shadowOpacity: 0.4,
        shadowRadius: 6,
        elevation: 5,
    },
});
