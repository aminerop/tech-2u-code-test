import * as React from "react";
import {View, Text, StyleSheet, TouchableOpacity} from "react-native";
import {User} from "core/types";
import FastImage from "react-native-fast-image";
import {useNavigation} from "@react-navigation/native";
import {AppNavigation} from "routes/types";
import Card from "components/Card";
import Skeleton from "components/Skeleton";
import Animated, {
    LightSpeedInLeft,
    Layout,
    LightSpeedOutRight,
} from "react-native-reanimated";

const Row = ({user}: {user?: User}) => {
    const {id, name, email} = user || {};
    const navigation = useNavigation<AppNavigation>();

    const onPress = () => {
        id && navigation.navigate("Details", {id});
    };

    return (
        <Animated.View
            style={{flex: 1}}
            entering={LightSpeedInLeft}
            layout={Layout.springify()}
            exiting={LightSpeedOutRight}>
            <Card style={styles.container} onPress={onPress}>
                <Skeleton show={!id} style={styles.avatar}>
                    <FastImage
                        style={styles.avatar}
                        source={{
                            uri: id
                                ? `https://randomuser.me/api/portraits/${
                                      id % 2 === 0 ? "men" : "women"
                                  }/${id}.jpg`
                                : "",
                        }}
                    />
                </Skeleton>
                <View style={styles.description}>
                    <Skeleton show={!name} height={15} width={"60%"} spaceY={6}>
                        <Text style={styles.name}>{name}</Text>
                    </Skeleton>
                    <Skeleton show={!email} height={15} width={"40%"}>
                        <Text style={styles.email}>{email}</Text>
                    </Skeleton>
                </View>
            </Card>
        </Animated.View>
    );
};

const styles = StyleSheet.create({
    container: {
        marginBottom: 16,
        flexDirection: "row",
        backgroundColor: "white",
    },
    avatar: {
        width: 50,
        height: 50,
        borderRadius: 25,
    },
    description: {
        flex: 1,
        marginHorizontal: 8,
        flexDirection: "column",
    },
    name: {
        fontSize: 21,
        fontWeight: "bold",
    },
    email: {
        fontSize: 16,
        opacity: 0.7,
    },
});

export default Row;
